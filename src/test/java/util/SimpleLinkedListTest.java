package util;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;

@DisplayName("Tests for SimpleLinkedList implementation")
public class SimpleLinkedListTest {
    private static String zero, one, two, three, fiveNull, six, nine;
    private LinkedList<String> startList;
    private SimpleLinkedList<String> simpleList;

    @BeforeAll
    public static void setUpStaticVariables(){
        zero = "zero";
        one = "one";
        two = "two";
        three = "three";
        fiveNull = null;
        six = "six";
        nine = "nine";
    }

    @BeforeEach
    public void init() {
        startList =
                new LinkedList<>(Arrays.asList(zero, one, two, three, fiveNull, null, six, three, null, nine));
        simpleList = new SimpleLinkedList<>(startList);
    }

    @Test
    public void sizeTest() {
        assertEquals(simpleList.size(), startList.size());
    }

    @Test
    public void isEmptyForEmptyListTest() {
        simpleList = new SimpleLinkedList<>();
        boolean expected = simpleList.isEmpty();
        assertTrue(expected);
    }

    @Test
    public void isEmptyForNotEmptyListTest() {
        boolean expected = simpleList.isEmpty();
        assertFalse(expected);
    }

    @Test
    public void containsTest() {
        boolean expected = simpleList.contains("two");
        assertTrue(expected);
    }

    @Test
    public void indexOfContainingObjectTest() {
        int expected = startList.indexOf("two");
        int actual = simpleList.indexOf("two");
        Iterator<String> it = startList.iterator();
        assertEquals(expected, actual);
    }

    @Test
    public void lastIndexOfNullableObjectTest() {
        int expected = startList.lastIndexOf(fiveNull);
        int actual = simpleList.lastIndexOf(fiveNull);
        assertEquals(expected, actual);
    }

    @Test
    public void addObjectIsTrueTest() {
        boolean addExpectedTrue = simpleList.add(two);
        assertTrue(addExpectedTrue);
    }

    @Test
    public void addObjectIsSameAddInStartingListTest() {
        simpleList.add(two);
        startList.add(two);
        assertEquals(simpleList.getLast(), startList.getLast());
    }

    @Test
    public void addAllToSimpleListIsTrueTest() {
        List<String> consumed = Arrays.asList("more", "less", "infinity");
        boolean expected = simpleList.addAll(consumed);
        assertTrue(expected);
    }

    @Test
    public void addAllToSimpleAndStartListIsEqualsTest() {
        List<String> consumed = Arrays.asList("more", "less", "infinity");
        simpleList.addAll(consumed);
        startList.addAll(consumed);
        assertArrayEquals(simpleList.stream().filter(Objects::nonNull).sorted().toArray(),
                startList.stream().filter(Objects::nonNull).sorted().toArray());
    }

    @Test
    public void containsAllOfAvailableInSimpleListElementsTest() {
        List<String> availableObjects = Arrays.asList(one, fiveNull, six);
        boolean expected = simpleList.containsAll(availableObjects);
        assertTrue(expected);
    }

    @Test
    public void removeAllOfAvailableElementsFromSimpleListTest() {
        List<String> availableObjects = Arrays.asList(one, fiveNull, six);
        boolean expected = simpleList.removeAll(availableObjects);
        assertTrue(expected);
    }

    @Test
    public void retainAllInSimpleListSameInLinkedListTest() {
        List<String> keepingCollection =
                Arrays.asList("hello", one, fiveNull, three, null, six);
        startList.retainAll(keepingCollection);
        simpleList.retainAll(keepingCollection);
        assertArrayEquals(simpleList.toArray(), startList.toArray());
    }

    @Test
    public void clearSimpleListTest() {
        simpleList.clear();
        Integer expected = 0;
        Integer actual = simpleList.size();
        assertEquals(expected, actual);
    }

    @Test
    public void getFromSimpleListAndFromLinkedListIsEqualsTest() {
        int index = 3;
        String expected = startList.get(index);
        String actual = simpleList.get(index);
        assertEquals(expected, actual);
    }

    @Test
    public void setElementToSimpleListAndReturnByIndexAvailableElemTest() {
        String settingElem = "kuku";
        int index = 2;
        String expected = simpleList.get(index);
        String actual = simpleList.set(index, settingElem);
        assertEquals(expected, actual);
    }

    @Test
    public void addStringToSimpleListTrueTest() {
        String element = "element";
        boolean expected = simpleList.add(element);
        assertTrue(expected);
    }

    @DisplayName("Tests removeLastOccurrence complex")
    @Test
    public void removeLastOccurrenceTrueTest() {
        String object = three;
        int firstOccurrence = simpleList.indexOf(object);
        int lastOccurrence = simpleList.lastIndexOf(object);
        boolean expected = simpleList.removeLastOccurrence(object);
        int lastOccurrenceAfter = simpleList.lastIndexOf(object);
        assertTrue((firstOccurrence != lastOccurrence) &&
                expected &&
                (firstOccurrence == lastOccurrenceAfter));
    }


}