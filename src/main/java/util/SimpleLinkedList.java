package util;

import java.util.*;
import java.util.stream.Stream;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * Doubly-linked list implementation of the {@code List} and {@code Deque}
 * interfaces.  Implements all optional list operations, and permits all
 * elements (including {@code null}).
 *
 * <p>All of the operations perform as could be expected for a doubly-linked
 * list.  Operations that index into the list will traverse the list from
 * the beginning or the end, whichever is closer to the specified index.
 *
 * <p>This list is not supported next {@link Collections# Collections} methods:
 * {@code toArray(T[] a)}, {@code listIterator()}, {@code listIterator(int index)},
 * {@code subList(int fromIndex, int toIndex)}, {@code descendingIterator()}, and throw
 * {@link UnsupportedOperationException()}.
 *
 * @param <E> the type of elements held in this collection
 * @author Alexey Pavlyuchenkov
 * @see List
 * @see ArrayList
 */

public final class SimpleLinkedList<E> implements List<E>, Deque<E> {

    private int size = 0;

    /**
     * Pointer to first node.
     */
    private Node<E> first;

    /**
     * Pointer to last node.
     */
    private Node<E> last;

    /**
     * Constructs an empty list.
     */
    public SimpleLinkedList() {
    }

    /**
     * Constructs a list containing the elements of the specified
     * collection, in the order they are returned by the collection's
     * iterator.
     *
     * @param collection the collection whose elements are to be placed into this list
     * @throws NullPointerException if the specified collection is null
     */
    public SimpleLinkedList(Collection<? extends E> collection) {
        this();
        addAll(collection);
    }

    /**
     * Returns the number of elements in this list.  If this list contains
     * more than {@code Integer.MAX_VALUE} elements, returns
     * {@code Integer.MAX_VALUE}.
     *
     * @return the number of elements in this list
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Returns {@code true} if this list contains no elements.
     *
     * @return {@code true} if this list contains no elements
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Returns {@code true} if this list contains the specified element.
     * More formally, returns {@code true} if and only if this list contains
     * at least one element {@code e} such that {@code Objects.equals(o, e)}.
     *
     * @param o element whose presence in this list is to be tested
     * @return {@code true} if this list contains the specified element
     */
    @Override
    public boolean contains(Object o) {
        return indexOf(o) > 0;
    }

    /**
     * Returns the index of the first occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     * More formally, returns the lowest index {@code i} such that
     * {@code Objects.equals(o, get(i))}, or -1 if there is no such index.
     *
     * @param o element to search for
     * @return the index of the first occurrence of the specified element in
     * this list, or -1 if this list does not contain the element
     */
    @Override
    public int indexOf(Object o) {
        Node<E> current = first;
        for (int i = 0; i < size; i++) {
            E content = current.content;
            if (Objects.equals(o, content)) {
                return i;
            }
            current = current.next;
        }
        return -1;
    }

    /**
     * Returns the index of the last occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     * More formally, returns the highest index {@code i} such that
     * {@code Objects.equals(o, get(i))}, or -1 if there is no such index.
     *
     * @param o element to search for
     * @return the index of the last occurrence of the specified element in
     * this list, or -1 if this list does not contain the element
     */
    @Override
    public int lastIndexOf(Object o) {
        Node<E> current = last;
        for (int i = size - 1; i >= 0; i--) {
            E content = current.content;
            if (Objects.equals(o, content)) {
                return i;
            }
            current = current.prev;
        }
        return -1;
    }


    /**
     * Appends the specified element to the end of this list.
     *
     * @param e element to be appended to this list
     * @return {@code true} (as specified by {@link Collection#add})
     */
    @Override
    public boolean add(E e) {
        addLast(e);
        return Objects.equals(e, last.content);
    }

    /**
     * Appends all of the elements in the specified collection to the end of
     * this list, in the order that they are returned by the specified
     * collection's iterator (optional operation).  The behavior of this
     * operation is undefined if the specified collection is modified while
     * the operation is in progress.  (Note that this will occur if the
     * specified collection is this list, and it's nonempty.)
     *
     * @param c collection containing elements to be added to this list
     * @return {@code true} if this list changed as a result of the call
     * @throws NullPointerException if the specified collection is null
     * @see #add(Object)
     */
    @Override
    public boolean addAll(Collection<? extends E> c) {
        add(null);
        boolean addCollection = addAll(size - 1, c);
        return addCollection && isNull(removeLast());
    }

    /**
     * Inserts all of the elements in the specified collection into this
     * list at the specified position.  Shifts the
     * element currently at that position (if any) and any subsequent
     * elements to the right (increases their indices).  The new elements
     * will appear in this list in the order that they are returned by the
     * specified collection's iterator.  The behavior of this operation is
     * undefined if the specified collection is modified while the
     * operation is in progress.  (Note that this will occur if the specified
     * collection is this list, and it's nonempty.)
     *
     * @param index index at which to insert the first element from the
     *              specified collection
     * @param c     collection containing elements to be added to this list
     * @return {@code true} if this list changed as a result of the call
     * @throws IndexOutOfBoundsException if the index is out of range
     *                                   ({@code index < 0 || index > size()})
     */
    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        indexInSizeRange(index);
        if (nonNull(c) && c.size() != 0) {
            Node<E> nodeFromIndex = nodeOf(index);
            for (E element : c) {
                linkBefore(nodeFromIndex, element);
            }
            return true;
        }
        return false;
    }

    /**
     * Returns {@code true} if this list contains all of the elements of the
     * specified collection.
     *
     * @param c collection to be checked for containment in this list
     * @return {@code true} if this list contains all of the elements of the
     * specified collection
     * @see #contains(Object)
     */
    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Removes from this list all of its elements that are contained in the
     * specified collection.
     *
     * @param c collection containing elements to be removed from this list
     * @return {@code true} if this list changed as a result of the call
     * @see #remove(Object)
     * @see #contains(Object)
     */
    @Override
    public boolean removeAll(Collection<?> c) {
        Objects.requireNonNull(c);
        boolean modified = false;
        for (Object object : c) {
            if (remove(object)) {
                modified = true;
            }
        }
        return modified;
    }

    /**
     * Retains only the elements in this list that are contained in the
     * specified collection.  In other words, removes
     * from this list all of its elements that are not contained in the
     * specified collection.
     *
     * @param c collection containing elements to be retained in this list
     * @return {@code true} if this list changed as a result of the call
     * @see #remove(Object)
     * @see #contains(Object)
     */
    @Override
    public boolean retainAll(Collection<?> c) {
        Objects.requireNonNull(c);
        boolean modified = false;
        Iterator<E> iterator = this.iterator();
        while (iterator.hasNext()) {
            E elem = iterator.next();
            if (nonNull(elem) && !c.contains(elem)) {
                iterator.remove();
                modified = true;
            }
        }
        return modified;
    }

    /**
     * Removes all of the elements from this list.
     * The list will be empty after this call returns.
     */
    @Override
    public void clear() {
        while (size != 0) {
            unlink(last);
        }
        first = last = null;
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of the element to return
     * @return the element at the specified position in this list
     * @throws IndexOutOfBoundsException if the index is out of range
     *                                   ({@code index < 0 || index >= size()})
     */
    @Override
    public E get(int index) {
        indexInSizeRange(index);
        return nodeOf(index).content;
    }

    /**
     * Replaces the element at the specified position in this list with the
     * specified element.
     *
     * @param index   index of the element to replace
     * @param element element to be stored at the specified position
     * @return the element previously at the specified position
     * @throws IndexOutOfBoundsException if the index is out of range
     *                                   ({@code index < 0 || index >= size()})
     */
    @Override
    public E set(int index, E element) {
        indexInSizeRange(index);
        Node<E> indexNode = nodeOf(index);
        E returned = indexNode.content;
        indexNode.content = element;
        return returned;
    }

    /**
     * Inserts the specified element at the specified position in this list.
     * Shifts the element currently at that position
     * (if any) and any subsequent elements to the right (adds one to their
     * indices).
     *
     * @param index   index at which the specified element is to be inserted
     * @param element element to be inserted
     * @throws IndexOutOfBoundsException if the index is out of range
     *                                   ({@code index < 0 || index > size()})
     */
    @Override
    public void add(int index, E element) {
        indexInSizeRange(index);
        Node<E> current = nodeOf(index);
        linkBefore(current, element);
    }

    /**
     * Inserts the specified element at the first position of this List.
     *
     * @param e the element to add
     */
    @Override
    public void addFirst(E e) {
        linkFirst(e);
    }

    /**
     * Inserts the specified element at the end of this List.
     *
     * <p>This method is equivalent to {@link #add}.
     *
     * @param e the element to add
     */
    @Override
    public void addLast(E e) {
        linkLast(e);
    }

    /**
     * Inserts the specified element at the front of this List.
     *
     * @param e the element to add
     * @return {@code true} if the element was added to this list, else
     * {@code false}
     */
    @Override
    public boolean offerFirst(E e) {
        addFirst(e);
        return Objects.equals(e, first.content);
    }


    /**
     * Inserts the specified element at the end of this list .
     *
     * @param e the element to add
     * @return {@code true} if the element was added to this list, else
     * {@code false}
     */
    @Override
    public boolean offerLast(E e) {
        addLast(e);
        return Objects.equals(e, last.content);
    }

    /**
     * Inserts the specified element into the queue represented by this list
     * (in other words, at the last position of this list) if it is possible to do so
     * immediately without violating capacity restrictions, returning
     * {@code true} upon success and {@code false} if no space is currently
     * available.
     *
     * <p>This method is equivalent to {@link #offerLast}.
     *
     * @param e the element to add
     * @return {@code true} if the element was added to this list, else
     * {@code false}
     */
    @Override
    public boolean offer(E e) {
        return offerLast(e);
    }

    /**
     * Pushes an element onto the stack represented by this list (in other
     * words, at the first position of this list).
     *
     * <p>This method is equivalent to {@link #addFirst}.
     *
     * @param e the element to push
     */
    @Override
    public void push(E e) {
        addFirst(e);
    }

    /**
     * Pops an element from the stack represented by this list.  In other
     * words, removes and returns the first element of this list.
     *
     * <p>This method is equivalent to {@link #removeFirst()}.
     *
     * @return the element at the front of this list (which is the top
     * of the stack represented by this list)
     * @throws NoSuchElementException if this list is empty
     */
    @Override
    public E pop() {
        return removeFirst();
    }


    /**
     * Retrieves, but does not remove, the first element of this list.
     * <p>
     * This method differs from {@link #peekFirst peekFirst} only in that it
     * throws an exception if this list is empty.
     *
     * @return the first element of this list
     * @throws NoSuchElementException if this list is empty
     */
    @Override
    public E getFirst() {
        if (isNull(first)) {
            throw new NoSuchElementException();
        }
        return first.content;
    }

    /**
     * Retrieves, but does not remove, the last element of this list.
     * This method differs from {@link #peekLast peekLast} only in that it
     * throws an exception if this list is empty.
     *
     * @return the last element of this deque
     * @throws NoSuchElementException if this list is empty
     */
    @Override
    public E getLast() {
        if (last == null) {
            throw new NoSuchElementException();
        }
        return last.content;
    }

    /**
     * Retrieves, but does not remove, the first element of this list,
     * or returns {@code null} if this list is empty.
     *
     * @return the first element of this list, or {@code null} if this list is empty
     */
    @Override
    public E peekFirst() {
        return isNull(first) ? null : first.content;
    }

    /**
     * Retrieves, but does not remove, the last element of this list,
     * or returns {@code null} if this list is empty.
     *
     * @return the last element of this list, or {@code null} if this list is empty
     */
    @Override
    public E peekLast() {
        return isNull(last) ? null : last.content;
    }

    /**
     * Retrieves, but does not remove, the head of the queue represented by
     * this list (in other words, the first element of this list).
     * This method differs from {@link #peek peek} only in that it throws an
     * exception if this list is empty.
     *
     * <p>This method is equivalent to {@link #getFirst()}.
     *
     * @return the head of the queue represented by this list
     * @throws NoSuchElementException if this list is empty
     */
    @Override
    public E element() {
        if (nonNull(first)) {
            return peek();
        }
        throw new NoSuchElementException();
    }

    /**
     * Retrieves, but does not remove, the head of the queue represented by
     * this list (in other words, the first element of this list), or
     * returns {@code null} if this list is empty.
     *
     * <p>This method is equivalent to {@link #peekFirst()}.
     *
     * @return the head of the queue represented by this deque, or
     * {@code null} if this deque is empty
     */
    @Override
    public E peek() {
        return peekFirst();
    }


    /**
     * Removes the first occurrence of the specified element from this list.
     * If this list does not contain the element, it is unchanged.
     * More formally, removes the element with the lowest index {@code i} such that
     * {@code Objects.equals(o, get(i))} (if such an element exists).
     * Returns {@code true} if this list contained the specified element
     * (or equivalently, if this list changed as a result of the call).
     *
     * @param o element to be removed from this list, if present
     * @return {@code true} if this list contained the specified element
     */
    @Override
    public boolean remove(Object o) {
        return removeFirstOccurrence(o);
    }

    /**
     * Retrieves and removes the first element by this List
     * This method differs from {@link #poll() poll()} only in that it
     * throws an exception if this List is empty.
     *
     * <p>This method is equivalent to {@link #removeFirst()}.
     *
     * @return the head of the queue represented by this deque
     * @throws NoSuchElementException if this deque is empty
     */
    @Override
    public E remove() {
        return removeFirst();
    }

    /**
     * Removes the element at the specified position in this list.
     * Shifts any subsequent elements to the left (subtracts one
     * from their indices).  Returns the element that was removed from the
     * list.
     *
     * @param index the index of the element to be removed
     * @return the element previously at the specified position
     * @throws IndexOutOfBoundsException if the index is out of range
     *                                   ({@code index < 0 || index >= size()})
     */
    @Override
    public E remove(int index) {
        indexInSizeRange(index);
        return unlink(nodeOf(index));
    }

    /**
     * Retrieves and removes the first element of this list.  This method
     * differs from {@link #pollFirst pollFirst} only in that it throws an
     * exception if this list is empty.
     *
     * @return the head of this deque
     * @throws NoSuchElementException if this list is empty
     */
    @Override
    public E removeFirst() {
        if (nonNull(first)) {
            return pollFirst();
        }
        throw new NoSuchElementException();
    }

    /**
     * Retrieves and removes the last element of this list.  This method
     * differs from {@link #pollLast pollLast} only in that it throws an
     * exception if this list is empty.
     *
     * @return the tail of this deque
     * @throws NoSuchElementException if this deque is empty
     */
    @Override
    public E removeLast() {
        if (nonNull(last)) {
            return pollLast();
        }
        throw new NoSuchElementException();
    }

    /**
     * Removes the first occurrence of the specified element from this list.
     * If the list does not contain the element, it is unchanged.
     * More formally, removes the first element {@code e} such that
     * {@code Objects.equals(o, e)} (if such an element exists).
     * Returns {@code true} if this list contained the specified element
     * (or equivalently, if this list changed as a result of the call).
     *
     * @param o element to be removed from this list, if present
     * @return {@code true} if an element was removed as a result of this call
     */
    @Override
    public boolean removeFirstOccurrence(Object o) {
        Optional<Node<E>> node = nodeOf(o);
        if (node.isPresent()) {
            unlink(node.get());
            return true;
        }
        return false;
    }

    /**
     * Removes the last occurrence of the specified element from this list.
     * If the list does not contain the element, it is unchanged.
     * More formally, removes the last element {@code e} such that
     * {@code Objects.equals(o, e)} (if such an element exists).
     * Returns {@code true} if this list contained the specified element
     * (or equivalently, if this list changed as a result of the call).
     *
     * @param o element to be removed from this list, if present
     * @return {@code true} if an element was removed as a result of this call
     */
    @Override
    public boolean removeLastOccurrence(Object o) {
        Optional<Node<E>> removedNode = nodeLastOccurrence(o);
        if (removedNode.isPresent()) {
            unlink(removedNode.get());
            return true;
        }
        return false;
    }

    /**
     * Retrieves and removes the first element of this list,
     * or returns {@code null} if this list is empty.
     *
     * @return the first element of this list, or {@code null} if this list is empty
     */
    @Override
    public E pollFirst() {
        return first == null ? null : unlink(first);
    }

    /**
     * Retrieves and removes the last element of this list,
     * or returns {@code null} if this list is empty.
     *
     * @return the last element of this list, or {@code null} if this list is empty
     */
    @Override
    public E pollLast() {
        if (last == null) {
            return null;
        } else {
            return unlink(last);
        }
    }

    /**
     * Retrieves and removes the head (first element) of this list,
     * or returns {@code null} if this list is empty.
     *
     * <p>This method is equivalent to {@link #pollFirst()}.
     *
     * @return the first element of this list, or {@code null} if
     * this list is empty
     */
    @Override
    public E poll() {
        return pollFirst();
    }


    /**
     * Returns an iterator over the elements in this list in proper sequence.
     *
     * @return an iterator over the elements in this list in proper sequence
     */
    @Override
    public Iterator<E> iterator() {
        return new Itrtr();
    }

    private class Itrtr implements Iterator<E> {
        int cursor = 0;
        int lastReturned = -1;

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return cursor < size;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public E next() {
            try {
                int index = cursor;
                E next = get(index);
                lastReturned = index;
                cursor = index + 1;
                return next;
            } catch (IndexOutOfBoundsException e) {
                throw new NoSuchElementException();
            }
        }

        /**
         * Removes from the list the last element returned
         * by this iterator.  This method can be called
         * only once per call to {@link #next}.
         * <p>
         * The behavior of an iterator is unspecified if the underlying collection
         * is modified while the iteration is in progress in any way other than by
         * calling this method, unless an overriding class has specified a
         * concurrent modification policy.
         * <p>
         * The behavior of an iterator is unspecified if this method is called
         * after a call to the {@link #forEachRemaining forEachRemaining} method.
         *
         * @throws IllegalStateException if the {@code next} method has not
         *                               yet been called, or the {@code remove} method has already
         *                               been called after the last call to the {@code next}
         *                               method
         */
        @Override
        public void remove() {
            if (lastReturned < 0) {
                throw new IllegalStateException();
            }
            SimpleLinkedList.this.remove(lastReturned);
            if (lastReturned < cursor) {
                cursor--;
            }
            lastReturned = -1;
        }
    }

    /**
     * Returns an array containing all of the elements in this list in proper
     * sequence (from first to last element).
     *
     * <p>This method acts as bridge between array-based and collection-based
     * APIs.
     *
     * @return an array containing all of the elements in this list in proper
     * sequence
     * @see Arrays#asList(Object[])
     */
    @Override
    public Object[] toArray() {
        Object[] returned = new Object[size];
        int i = 0;
        for (Node<E> currentNode = first;
             currentNode != null;
             currentNode = currentNode.next) {
            returned[i++] = currentNode.content;
        }
        return returned;
    }

    /**
     * Unsupported methods...
     *
     * @throws UnsupportedOperationException
     */
    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ListIterator<E> listIterator() {
        throw new UnsupportedOperationException();
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<E> descendingIterator() {
        throw new UnsupportedOperationException();
    }


    /* private methods
     */

    private static class Node<E> {
        E content;
        Node<E> prev;
        Node<E> next;

        public Node(Node<E> prev, E content, Node<E> next) {
            this.content = content;
            this.prev = prev;
            this.next = next;
        }
    }

    private Node<E> nodeOf(int index) {
        Node<E> required = first;
        if (index <= (size / 2)) {
            for (int i = 0; i < index; i++) {
                required = required.next;
            }
        } else {
            required = last;
            for (int i = size - 1; i > index; i--) {
                required = required.prev;
            }
        }
        return required;
    }

    private Optional<Node<E>> nodeOf(Object object) {
        return Stream.iterate(first, node -> node.next)
                .limit(size)
                .filter(node -> Objects.equals(object, node.content))
                .findFirst();
    }

    private Optional<Node<E>> nodeLastOccurrence(Object object) {
        return Stream.iterate(last, node -> node.prev)
                .limit(size)
                .filter(node -> Objects.equals(object, node.content))
                .findFirst();
    }


    private void indexInSizeRange(int index) {
        Objects.checkIndex(index, size);
    }

    private void linkFirst(E e) {
        Node<E> willSecond = first;
        Node<E> newNode = new Node<>(null, e, willSecond);
        first = newNode;
        if (willSecond == null) {
            last = newNode;
        } else {
            willSecond.prev = newNode;
        }
        size++;
    }

    private void linkLast(E e) {
        Node<E> willBeforeLast = last;
        Node<E> newNode = new Node<>(willBeforeLast, e, null);
        last = newNode;
        if (willBeforeLast == null) {
            first = newNode;
        } else {
            willBeforeLast.next = newNode;
        }
        size++;
    }

    private void linkBefore(Node<E> current, E e) {
        if (isNull(current)) {
            linkLast(e);
        } else {
            Node<E> previous = current.prev;
            Node<E> newNode = new Node<>(previous, e, current);
            if (nonNull(previous)) {
                previous.next = newNode;
                current.prev = newNode;
            } else {
                first = newNode;
                current.prev = first;
            }
            size++;
        }
    }

    private E unlink(Node<E> current) {
        E element = current.content;
        Node<E> prev = current.prev;
        Node<E> next = current.next;

        if (isNull(prev)) {
            first = current.next;
        } else {
            prev.next = current.next;
            current.prev = null;
        }

        if (isNull(next)) {
            last = prev;
        } else {
            next.prev = prev;
            current.next = null;
        }
        current.content = null;
        size--;
        return element;
    }
}
